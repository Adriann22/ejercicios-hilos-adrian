class ATMMain {
    public static void main(String[] args) {
        ATM[] atms = new ATM[5];
        for (int i = 0; i < 5; i++) {
            atms[i] = new ATM("ATM" + (i + 1));
            atms[i].start();
        }
    }
}