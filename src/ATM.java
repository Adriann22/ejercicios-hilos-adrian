import java.util.Random;

public class ATM extends Thread{
    public String name;

    public ATM(String name) {
        this.name = name;
    }

    @Override
    public void run(){
        Random random = new Random();
        int tiempo =random.nextInt(8) + 3;

        for (int i = 0; i < tiempo; i++) {
            System.out.println("ATM " + name + " está en uso");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}