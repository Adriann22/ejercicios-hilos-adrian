import java.util.concurrent.Semaphore;

class Supermercado {
    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(2);
        Caja[] cajas = new Caja[3];
        cajas[0] = new Caja("Caja1", semaforo);
        cajas[1] = new Caja("Caja2", semaforo);
        cajas[2] = new Caja("Caja3", semaforo);

        CajaHilo hilo1 = new CajaHilo(cajas[0], "Cliente1", 3);
        CajaHilo hilo2 = new CajaHilo(cajas[1], "Cliente2", 2);
        CajaHilo hilo3 = new CajaHilo(cajas[2], "Cliente3", 4);

        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}