import java.util.Random;
import java.util.concurrent.Semaphore;

class Caja {
    public String nombre;
    public Semaphore semaforo;

    public Caja(String nombre, Semaphore semaforo) {
        this.nombre = nombre;
        this.semaforo = semaforo;
    }

    public void procesarCompra(String cliente, int productos) {
        System.out.println("La caja " + nombre + " recibe al cliente " + cliente);
        Random rand = new Random();
        try {
            semaforo.acquire();
            for (int i = 1; i <= productos; i++) {
                System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i);
                Thread.sleep(rand.nextInt(11));
            }
            semaforo.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
