class CajaHilo extends Thread {
    public Caja caja;
    public String cliente;
    public int productos;

    public CajaHilo(Caja caja, String cliente, int productos) {
        this.caja = caja;
        this.cliente = cliente;
        this.productos = productos;
    }

    @Override
    public void run() {
        caja.procesarCompra(cliente, productos);
    }
}